Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      get '/products/:country_code' => 'products#index'
      get '/products/' => 'products#index'
    end
  end
end
