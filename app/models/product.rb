class Product < ApplicationRecord

  def normalize_price country_code
    currency = Country.find_by(code: country_code).try(:currency)
    exchange_rate = currency.try(:exchange_rate) || 1
    self.price *= exchange_rate if currency
    self.price =  self.price.round(4)
  end
end
