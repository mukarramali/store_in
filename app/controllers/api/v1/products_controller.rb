class Api::V1::ProductsController < Api::ApiController

  def index
    @products = Product.all
    @products.map {|product| product.normalize_price country_code}
    render json: @products
  end

  private

  def country_code
    current_country
    params[:country_code] || current_country || 'US'
  end

  # Using the IP address of the request, we can determine the country code
  def current_country
    @geoip ||= GeoIP.new(Rails.root.join('db').join('GeoIP.dat'))
    remote_ip = request.remote_ip
    if remote_ip != "127.0.0.1" #todo: check for other local addresses or set default value
      country = @geoip.country(remote_ip)
      country.country_code2
    end
  end
end
