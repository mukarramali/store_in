def find_or_create(model, record_list)
  record_list.each do |attributes|
    model.find_or_create_by attributes
  end
end

Country.destroy_all
Currency.destroy_all
Product.destroy_all

find_or_create Currency, [
  { id: 1, name: 'Dollars', symbol: '$', exchange_rate: 1 },
  { id: 2, name: 'INR', symbol: '₹', exchange_rate: 71 },
  { id: 3, name: 'Euro', symbol: '‎€', exchange_rate: 0.86 },
  { id: 4, name: 'UAE Dirham', symbol: 'إ.د', exchange_rate: 3.67 },
  { id: 5, name: 'Pound', symbol: '£', exchange_rate: 0.78 },
  { id: 6, name: 'Yen', symbol: '¥', exchange_rate: 111 }
]
find_or_create Country, [
  { name: 'USA', code: 'US', currency_id: 1 },
  { name: 'United Kingdom', code: 'GB', currency_id: 5 },
  { name: 'UAE', code: 'AE', currency_id: 4 },
  { name: 'Australia', code: 'AU', currency_id: 1 },
  { name: 'Japan', code: 'JP', currency_id: 6 },
  { name: 'India', code: 'IN', currency_id: 2 },
  { name: 'Pakistan', code: 'PK', currency_id: 2 },
  { name: 'South Africa', code: 'ZA', currency_id: 1 },
  { name: 'Italy', code: 'IT', currency_id: 3 },
  { name: 'Bangladesh', code: 'BD', currency_id: 2 }
]
find_or_create Product, [
  { name: 'A Product', thumbnail: 'https://xyz.com', price: 111 },
  { name: 'B Product', thumbnail: 'https://xyz.com', price: 112 },
  { name: 'C Product', thumbnail: 'https://xyz.com', price: 113 },
  { name: 'D Product', thumbnail: 'https://xyz.com', price: 114 },
  { name: 'E Product', thumbnail: 'https://xyz.com', price: 115 },
  { name: 'F Product', thumbnail: 'https://xyz.com', price: 116 },
  { name: 'G Product', thumbnail: 'https://xyz.com', price: 117 },
  { name: 'H Product', thumbnail: 'https://xyz.com', price: 118 },
  { name: 'I Product', thumbnail: 'https://xyz.com', price: 119 },
  { name: 'J Product', thumbnail: 'https://xyz.com', price: 110 },
  { name: 'K Product', thumbnail: 'https://xyz.com', price: 119 },
  { name: 'L Product', thumbnail: 'https://xyz.com', price: 118 },
  { name: 'M Product', thumbnail: 'https://xyz.com', price: 117 },
  { name: 'N Product', thumbnail: 'https://xyz.com', price: 117 },
  { name: 'O Product', thumbnail: 'https://xyz.com', price: 116 },
  { name: 'P Product', thumbnail: 'https://xyz.com', price: 115},
  { name: 'Q Product', thumbnail: 'https://xyz.com', price: 114 },
  { name: 'R Product', thumbnail: 'https://xyz.com', price: 11 },
  { name: 'S Product', thumbnail: 'https://xyz.com', price: 11 }
]
