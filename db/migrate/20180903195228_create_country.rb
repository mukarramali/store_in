class CreateCountry < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :code
      t.string :name
      t.references :currency, foreign_key: true, index: true
      t.timestamps
    end
  end
end
