## DB setup

$ rake db:create

$ rake db:migrate

## Import seeds/sample data

$ rake db:seed
